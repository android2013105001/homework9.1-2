package resolution.example6.zzeulki.homework91;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
    }

    public void click(View v) {
        Intent i = new Intent();
        i.setAction("com.broadcast.ANDROID");
        sendBroadcast(i);
    }

}
